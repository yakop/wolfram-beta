package yg.wolframbeta.operations.simplification

import yg.wolframbeta.grammar.*
import yg.wolframbeta.operations.flatops.ExpressionUnflattener
import yg.wolframbeta.operations.flatops.FlatExpressionConverter
import yg.wolframbeta.operations.flatops.FlatMul
import yg.wolframbeta.operations.flatops.FlatSum
import yg.wolframbeta.operations.utils.isZero
import kotlin.math.pow

class ExpressionSimplifier {
  private val converter = FlatExpressionConverter()

  private val simplifierChain = listOf(
    PowEvaluator(this),
    ConstantMultiplicationSimplifier(),
    MultiplicationByZeroSimplifier(),
    ConstantSumSimplifier(),
    VariableFoldingSimplifier(),
    MultiplicationByOneSimplifier(),
    MinusZeroSimplifier(),
    SortSimplifier()
  )

  private val unflattener = ExpressionUnflattener()

  fun simplify(expression: Expression) =
    converter.convert(expression).expressions
      .map { it.expressions }
      .let { runSimplifiers(it) }
      .let { unflattener.unflat(it) }

  private fun runSimplifiers(flatExpression: List<List<Expression>>) =
    FlatSum(
      simplifierChain
        .fold(flatExpression) { acc, simplifier -> simplifier.simplify(acc) }
        .map { FlatMul(it) }
    )
}

interface FlatExpressionSimplifier {
  fun simplify(expressions: List<List<Expression>>): List<List<Expression>>
}

class ConstantMultiplicationSimplifier : FlatExpressionSimplifier {
  override fun simplify(expressions: List<List<Expression>>) =
    expressions
      .map { multiplication -> foldConstantsWithinMultiplication(multiplication) }

  private fun foldConstantsWithinMultiplication(multiplication: List<Expression>) =
    multiplication
      .groupBy { it is Const }
      .map { (isConst, values) ->
        if (isConst) {
          foldConstants(values.map { it as Const })
        } else {
          values
        }
      }
      .flatten()

  private fun foldConstants(values: List<Const>) =
    listOf(
      values.reduce { c1, c2 -> Const(c1.value * c2.value) }
    )
}

class ConstantSumSimplifier : FlatExpressionSimplifier {
  override fun simplify(expressions: List<List<Expression>>) =
    getNonConstantExpressions(expressions) +
        getPossiblyConstantExpressions(expressions)
          .groupBy { it is Const }
          .map { (isConst, values) ->
            if (isConst) {
              foldConstants(values.map { it as Const })
            } else {
              values.map { listOf(it) }
            }
          }
          .flatten()

  private fun getNonConstantExpressions(expressions: List<List<Expression>>) =
    expressions.filter { it.size > 1 }

  private fun getPossiblyConstantExpressions(expressions: List<List<Expression>>) =
    expressions.filter { it.size == 1 }.map { it.first() }

  private fun foldConstants(values: List<Const>): List<List<Expression>> =
    listOf(listOf(values.reduce { c1, c2 -> Const(c1.value + c2.value) }))
}

class MultiplicationByZeroSimplifier : FlatExpressionSimplifier {
  override fun simplify(expressions: List<List<Expression>>) =
    expressions
      .filterNot { it.contains(Const(0.0)) }
}

class MultiplicationByOneSimplifier : FlatExpressionSimplifier {
  override fun simplify(expressions: List<List<Expression>>) =
    expressions
      .map { multiplication -> deleteOnesWithinMultiplications(multiplication) }

  private fun deleteOnesWithinMultiplications(multiplication: List<Expression>) =
    if (multiplication.size > 1) {
      multiplication.filterNot { it == Const(1.0) }
    } else {
      multiplication
    }
}

class VariableFoldingSimplifier : FlatExpressionSimplifier {
  override fun simplify(expressions: List<List<Expression>>) =
    groupByCommonNonConstantPart(expressions)
      .map { (commonPart, multiplications) ->
        commonPart + foldConstants(extractConstantParts(multiplications, commonPart))
      }

  private fun groupByCommonNonConstantPart(expressions: List<List<Expression>>) =
    expressions
      .groupBy { multiplication ->
        multiplication.filterNot { it is Const }
      }

  private fun foldConstants(constantParts: List<Const>) =
    constantParts.reduce { c1, c2 -> Const(c1.value + c2.value) }

  private fun extractConstantParts(
    multiplications: List<List<Expression>>,
    commonPart: List<Expression>
  ) = multiplications
    .map { it - commonPart }
    .map { it.firstOrNull() as? Const ?: Const(1.0) }
}

class MinusZeroSimplifier : FlatExpressionSimplifier {
  override fun simplify(expressions: List<List<Expression>>) =
    expressions
      .groupBy { it.size == 1 }
      .map { (isOneElement, expressions) ->
        if (isOneElement) {
          replaceMinusZeroByZero(expressions)
        } else {
          expressions
        }
      }.flatten()

  private fun replaceMinusZeroByZero(expressions: List<List<Expression>>) =
    expressions.map {
      if (it.first() == Const(-0.0)) {
        listOf(Const(0.0))
      } else {
        it
      }
    }
}

class SortSimplifier : FlatExpressionSimplifier {
  override fun simplify(expressions: List<List<Expression>>) =
    sortMultiplications(expressions)
      .map { sortWithinExpression(it) }

  private fun sortMultiplications(expressions: List<List<Expression>>) =
    expressions
      .sortedWith(
        Comparator { mulChain1, mulChain2 -> orderByPower(mulChain2, mulChain1) }
      )

  private fun orderByPower(mulChain2: List<Expression>, mulChain1: List<Expression>): Int {
    val diff = mulChain2.size - mulChain1.size

    return when {
      diff != 0 -> diff
      else -> power(mulChain2) - power(mulChain1)
    }
  }

  private fun power(mulChain: List<Expression>) = mulChain.count { it is Variable }

  private fun sortWithinExpression(expression: List<Expression>) =
    expression.sortedWith(
      Comparator { expression1, expression2 ->
        value(expression1) - value(expression2)
      }
    )

  private fun value(expression: Expression): Int {
    return when (expression) {
      is Const -> -1
      is Variable -> expression.name.chars().sum()
      is PredefinedFunction -> expression.function.name.chars().sum()
      is Pow -> value(expression.base)
      else -> throw IllegalStateException("Operation for type ${expression.javaClass} is not implemented")
    }
  }
}

class PowEvaluator(private val simplifier: ExpressionSimplifier) : FlatExpressionSimplifier {

  override fun simplify(expressions: List<List<Expression>>): List<List<Expression>> {
    return expressions
      .map { multiplication ->
        multiplication
          .groupBy { it is Pow }
          .map { (isPow, expressions) ->
            if (isPow) {
              expressions.map { processPow(it as Pow) }
            } else {
              expressions
            }
          }
          .flatten()
      }
  }

  private fun processPow(powExpression: Pow): Expression {
    val baseSimplified = simplifier.simplify(powExpression.base)
    val exponentSimplified = simplifier.simplify(powExpression.exponent)

    return if (baseSimplified is Const && exponentSimplified is Const) {
      Const(baseSimplified.value.pow(exponentSimplified.value))
    } else if (isZero(baseSimplified)) {
      Const(0.0)
    } else if (exponentSimplified == Const(1.0)) {
      baseSimplified
    } else {
      powExpression
    }
  }
}

