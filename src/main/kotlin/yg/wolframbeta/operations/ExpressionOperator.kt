package yg.wolframbeta.operations

import yg.wolframbeta.grammar.*
import yg.wolframbeta.grammar.Function

abstract class ExpressionOperator<T> {
  fun operate(expression: Expression) =
    when (expression) {
      is Sum -> operate(expression)
      is Diff -> operate(expression)
      is Mul -> operate(expression)
      is Div -> operate(expression)
      is Function -> operate(expression)
      is Neg -> operate(expression)
      is Invert -> operate(expression)
      is Variable -> operate(expression)
      is Const -> operate(expression)
      is Antiderivative -> operate(expression)
      is PredefinedFunction -> operate(expression)
      is Pow -> operate(expression)
    }

  abstract fun operate(sum: Sum): T
  abstract fun operate(diff: Diff): T
  abstract fun operate(mul: Mul): T
  abstract fun operate(div: Div): T
  abstract fun operate(function: Function): T
  abstract fun operate(negation: Neg): T
  abstract fun operate(inversion: Invert): T
  abstract fun operate(variable: Variable): T
  abstract fun operate(const: Const): T
  abstract fun operate(pow: Pow): T
  abstract fun operate(predefinedFunction: PredefinedFunction): T
  abstract fun operate(antiderivative: Antiderivative): T
}