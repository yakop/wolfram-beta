package yg.wolframbeta.operations.output

import yg.wolframbeta.grammar.*
import yg.wolframbeta.grammar.Function
import yg.wolframbeta.operations.ExpressionOperator

class AsciiMathPrinter : ExpressionOperator<String>() {
  override fun operate(sum: Sum) = "${operate(sum.left)} + ${operate(sum.right)}"

  override fun operate(diff: Diff) = "${operate(diff.left)} - ${operate(diff.right)}"

  override fun operate(mul: Mul) =
    "${surroundIfNeededForMulAndDiv(mul.left)} * ${surroundIfNeededForMulAndDiv(mul.right)}"

  override fun operate(div: Div) =
    "${surroundIfNeededForMulAndDiv(div.left)} / ${surroundIfNeededForMulAndDiv(div.right)}"

  private fun surroundIfNeededForMulAndDiv(expression: Expression) =
    when (expression) {
      is Sum, is Diff -> "(${operate(expression)})"
      else -> operate(expression)
    }

  override fun operate(function: Function): String {
    TODO("not implemented")
  }

  override fun operate(negation: Neg): String {
    return "- ${surroundIfNeededForPowAndNeg(negation.subExpression)}"
  }

  override fun operate(inversion: Invert): String {
    TODO("not implemented")
  }

  override fun operate(variable: Variable) = variable.name

  override fun operate(const: Const) = const.value.toString()

  override fun operate(pow: Pow) =
    "${surroundIfNeededForPowAndNeg(pow.base)} ^ ${surroundIfNeededForPowAndNeg(pow.exponent)}"

  private fun surroundIfNeededForPowAndNeg(expression: Expression) =
    when (expression) {
      is BinaryExpression -> "(${operate(expression)})"
      else -> operate(expression)
    }

  override fun operate(predefinedFunction: PredefinedFunction) =
    "${predefinedFunction.function}(${predefinedFunction.arguments.joinToString(",") { operate(it) }})"

  override fun operate(antiderivative: Antiderivative): String {
    TODO("not implemented")
  }
}