package yg.wolframbeta.operations.utils

import yg.wolframbeta.grammar.Const
import yg.wolframbeta.grammar.Expression

fun isZero(expression: Expression) = expression == Const(0.0)
