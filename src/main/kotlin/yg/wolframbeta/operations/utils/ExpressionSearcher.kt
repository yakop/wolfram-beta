package yg.wolframbeta.operations.utils

import yg.wolframbeta.grammar.*
import yg.wolframbeta.grammar.Function
import yg.wolframbeta.operations.ExpressionOperator

class ExpressionSearcher(val expression: Expression) : ExpressionOperator<Boolean>() {
  override fun operate(sum: Sum) = sum == expression || operate(sum.left) || operate(sum.right)

  override fun operate(diff: Diff) = diff == expression || operate(diff.left) || operate(diff.right)

  override fun operate(mul: Mul) = mul == expression || operate(mul.left) || operate(mul.right)

  override fun operate(div: Div) = div == expression || operate(div.left) || operate(div.right)

  override fun operate(function: Function) = function == expression || operate(function.expression)

  override fun operate(negation: Neg) = negation == expression || operate(negation.subExpression)

  override fun operate(inversion: Invert) =
    inversion == expression || inversion == expression || operate(inversion.subExpression)

  override fun operate(variable: Variable) = variable == expression

  override fun operate(const: Const) = const == expression

  override fun operate(pow: Pow) = pow == expression || operate(pow.base) || operate(pow.exponent)

  override fun operate(predefinedFunction: PredefinedFunction) =
    predefinedFunction == expression || predefinedFunction.arguments
      .map { operate(it) }
      .reduce { acc, b -> acc || b }

  override fun operate(antiderivative: Antiderivative): Boolean {
    return antiderivative == expression || operate(antiderivative.function)
  }
}
