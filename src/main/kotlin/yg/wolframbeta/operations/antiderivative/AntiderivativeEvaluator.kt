package yg.wolframbeta.operations.antiderivative

import yg.wolframbeta.grammar.*
import yg.wolframbeta.grammar.Function
import yg.wolframbeta.grammar.PredefinedFunction.PredefinedFunctions
import yg.wolframbeta.operations.flatops.ExpressionUnflattener
import yg.wolframbeta.operations.flatops.FlatExpressionConverter
import yg.wolframbeta.operations.flatops.FlatMul
import yg.wolframbeta.operations.flatops.FlatSum
import yg.wolframbeta.operations.simplification.ExpressionSimplifier
import yg.wolframbeta.operations.utils.ExpressionSearcher

class AntiderivativeEvaluator {
  private val converter = FlatExpressionConverter()
  private val simplifier = ExpressionSimplifier()
  private val unflattener = ExpressionUnflattener()
  private val failedAntiderivativeMessage = "Unable to find antiderivative for given function!"

  fun findAntiderivative(function: Function, variable: Variable) =
    findAntiderivativeOfFlatExpression(
      flatSum = converter.convert(function.expression),
      variable = variable
    ).let {
      unflattener.unflat(it)
    }.let { antiderivative ->
      simplifier.simplify(antiderivative).let {
        Function(
          simplifier.simplify(it),
          checkForVariable(it, variable)
        )
      }
    }

  private fun findAntiderivativeOfFlatExpression(flatSum: FlatSum, variable: Variable): FlatSum =
    FlatSum(
      flatSum.expressions
        .map { multiplication ->
          FlatMul(findAntiderivativeOfMultiplication(multiplication, variable))
        }
    )

  private fun findAntiderivativeOfMultiplication(multiplication: FlatMul, variable: Variable) =
    splitToConstAndNonConstPart(multiplication, variable)
      .map { (isConst, values) ->
        if (isConst) {
          findAntiderivativeOfConstantPartOfMultiplication(values, multiplication, variable)
        } else {
          findAntiderivativeOfNonConstantPartOfMultiplication(values, variable)
        }
      }
      .flatten()

  private fun findAntiderivativeOfConstantPartOfMultiplication(
    values: List<Expression>,
    multiplication: FlatMul,
    variable: Variable
  ) = if (multiplication.expressions == values) {
    multiplyConstByVariable(values, variable)
  } else {
    values
  }

  private fun multiplyConstByVariable(values: List<Expression>, variable: Variable) =
    values + variable

  private fun splitToConstAndNonConstPart(multiplication: FlatMul, variable: Variable) =
    multiplication.expressions
      .groupBy { it is Const || isNotAntiderivativeVariable(it, variable) }

  private fun isNotAntiderivativeVariable(expression: Expression, variable: Variable) =
    expression is Variable && expression != variable

  private fun findAntiderivativeOfNonConstantPartOfMultiplication(
    values: List<Expression>,
    variable: Variable
  ) = when {
    values.size == 1 -> findAntiderivativeOfOneElementExpression(values.first(), variable)
    else -> throw IllegalArgumentException(failedAntiderivativeMessage)
  }

  private fun findAntiderivativeOfOneElementExpression(expression: Expression, variable: Variable) =
    when (expression) {
      is Variable -> listOf(variable, variable)
      is PredefinedFunction -> getAntiderivativeOfTableFunction(expression, variable)
      is Pow -> getAntiderivativeOfPowExpression(expression, variable)
      else -> throw IllegalArgumentException(failedAntiderivativeMessage)
    }

  private fun getAntiderivativeOfTableFunction(function: PredefinedFunction, variable: Variable) =
    if (function.arguments.single() == variable) {
      when (function.function) {
        PredefinedFunctions.SIN -> listOf(
          Neg(PredefinedFunction(PredefinedFunctions.COS, function.arguments))
        )
        PredefinedFunctions.COS -> listOf(
          PredefinedFunction(PredefinedFunctions.SIN, function.arguments)
        )
      }
    } else {
      throw IllegalArgumentException(failedAntiderivativeMessage)
    }

  private fun getAntiderivativeOfPowExpression(pow: Pow, variable: Variable) =
    if (pow.base == variable) {
      if (pow.exponent is Const) {
        listOf(Const(1 / (pow.exponent.value + 1)), Pow(pow.base, Const(pow.exponent.value + 1)))
      } else {
        throw IllegalArgumentException(failedAntiderivativeMessage)
      }
    } else {
      throw IllegalArgumentException(failedAntiderivativeMessage)
    }

  private fun checkForVariable(expression: Expression, variable: Variable) =
    if (ExpressionSearcher(variable).operate(expression)) {
      mapOf(variable to null)
    } else {
      emptyMap()
    }
}