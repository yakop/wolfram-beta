package yg.wolframbeta.operations.flatops

import yg.wolframbeta.grammar.Expression

data class FlatSum(val expressions: List<FlatMul>)

//abstract class FlatExpression(open val expressions: List<Expression>)

data class FlatMul(val expressions: List<Expression>)/* : FlatExpression(expressions)*/