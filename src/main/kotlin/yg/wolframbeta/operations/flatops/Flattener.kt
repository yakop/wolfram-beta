package yg.wolframbeta.operations.flatops

import yg.wolframbeta.grammar.*
import yg.wolframbeta.grammar.Function
import yg.wolframbeta.operations.ExpressionOperator

class Flattener : ExpressionOperator<List<FlatMul>>() {

  override fun operate(sum: Sum) = operate(sum.left) + operate(sum.right)

  override fun operate(diff: Diff) = operate(diff.left) +
      operate(diff.right)
        .map { flatMul -> FlatMul(listOf(Const(-1.0)) + flatMul.expressions) }


  override fun operate(mul: Mul) =
    operate(mul.left)
      .map { firstMul ->
        operate(mul.right)
          .map { secondMul ->
            FlatMul(firstMul.expressions + secondMul.expressions)
          }
      }
      .flatten()

  override fun operate(div: Div) = TODO()

  override fun operate(negation: Neg): List<FlatMul> =
    operate(negation.subExpression).map { flatMul ->
      FlatMul(listOf(Const(-1.0)) + flatMul.expressions)
    }

  override fun operate(inversion: Invert) = TODO()

  override fun operate(function: Function) = listOf(FlatMul(listOf(function)))

  override fun operate(variable: Variable) = listOf(FlatMul(listOf(variable)))

  override fun operate(const: Const) = listOf(FlatMul(listOf(const)))

  override fun operate(pow: Pow) = listOf(FlatMul(listOf(pow)))

  override fun operate(predefinedFunction: PredefinedFunction) =
    listOf(FlatMul(listOf(predefinedFunction)))

  override fun operate(antiderivative: Antiderivative) =
    listOf(FlatMul(listOf(antiderivative)))
}
