package yg.wolframbeta.operations.flatops

import yg.wolframbeta.grammar.Const
import yg.wolframbeta.grammar.Expression
import yg.wolframbeta.grammar.Mul
import yg.wolframbeta.grammar.Sum

class ExpressionUnflattener {
  fun unflat(flatSum: FlatSum) =
    flatSum.expressions
      .map { foldMultiplication(it.expressions) }
      .let { toSimpleSum(it) }

  private fun foldMultiplication(multiplication: List<Expression>): Expression =
    if (multiplication.size > 1) {
      Mul(multiplication.first(), foldMultiplication(multiplication.drop(1)))
    } else {
      multiplication.first()
    }

  private fun toSimpleSum(sum: List<Expression>): Expression =
    when {
      sum.size > 1 -> Sum(sum.first(), toSimpleSum(sum.drop(1)))
      sum.size == 1 -> sum.first()
      else -> Const(0.0)
    }
}
