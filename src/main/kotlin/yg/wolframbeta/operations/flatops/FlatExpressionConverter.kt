package yg.wolframbeta.operations.flatops

import yg.wolframbeta.grammar.Expression

class FlatExpressionConverter {
  private val flattener = Flattener()

  fun convert(initialExpression: Expression) =
    FlatSum(flattener.operate(initialExpression))
}