package yg.wolframbeta.grammar

import yg.wolframbeta.compiler.Context
import yg.wolframbeta.operations.antiderivative.AntiderivativeEvaluator
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin

sealed class Expression : Statement {
  abstract fun evaluate(context: Context): Double
  fun evaluate() = evaluate(Context(emptyMap()))
  override fun execute(context: Context) = Const(evaluate(context)) to context
}

data class Antiderivative(val function: Function, val variable: Variable) : Expression() {
  private val evaluator = AntiderivativeEvaluator()

  override fun evaluate(context: Context) =
    evaluator.findAntiderivative(function, variable).evaluate(context)
}

data class Function(
  val expression: Expression,
  val arguments: Map<Variable, Expression?>
) : Expression() {
  override fun evaluate(context: Context) =
    expression.evaluate(variablesToContext(context))

  private fun variablesToContext(context: Context) =
    Context(
      arguments.map { (variable, expression) ->
        variable.name to (expression ?: context[variable.name])
      }.toMap()
    )
}

data class PredefinedFunction(
  val function: PredefinedFunctions,
  val arguments: List<Expression>
) : Expression() {
  override fun evaluate(context: Context) = function.evaluate(completeVariables(context))

  private fun completeVariables(context: Context) =
    arguments.map { expression ->
      when (expression) {
        is Variable -> context[expression.name]
        else -> expression
      }
    }

  enum class PredefinedFunctions {
    SIN {
      override fun evaluate(arguments: List<Expression>) = sin(arguments.single().evaluate())
    },

    COS {
      override fun evaluate(arguments: List<Expression>) = cos(arguments.single().evaluate())
    };

    abstract fun evaluate(arguments: List<Expression>): Double
  }
}

data class Pow(
  val base: Expression,
  val exponent: Expression
) : Expression() {
  override fun evaluate(context: Context) =
    base.evaluate(context).pow(exponent.evaluate(context))
}

sealed class BinaryExpression(
  open val left: Expression,
  open val right: Expression
) : Expression()

data class Mul(
  override val left: Expression,
  override val right: Expression
) : BinaryExpression(left, right) {

  override fun evaluate(context: Context) =
    left.evaluate(context) * right.evaluate(context)
}

data class Div(
  override val left: Expression,
  override val right: Expression
) : BinaryExpression(left, right) {

  override fun evaluate(context: Context) =
    left.evaluate(context) / right.evaluate(context)
}

data class Sum(
  override val left: Expression,
  override val right: Expression
) : BinaryExpression(left, right) {

  override fun evaluate(context: Context) =
    left.evaluate(context) + right.evaluate(context)
}

data class Diff(
  override val left: Expression,
  override val right: Expression
) : BinaryExpression(left, right) {
  override fun evaluate(context: Context) =
    left.evaluate(context) - right.evaluate(context)
}

data class Neg(val subExpression: Expression) : Expression() {
  override fun evaluate(context: Context): Double =
    subExpression.evaluate(context).let {
      if (it == 0.0 || it == -0.0) {
        0.0
      } else {
        -it
      }
    }
}

data class Invert(val subExpression: Expression) : Expression() {
  override fun evaluate(context: Context): Double = 1.0 / subExpression.evaluate(context)
}

data class Variable(val name: String) : Expression() {
  override fun evaluate(context: Context) = context[name].evaluate(context)
}

data class Const(val value: Double) : Expression() {
  override fun evaluate(context: Context): Double = value

  override fun equals(other: Any?) =
    if (other is Const) {
      abs(other.value - this.value) < 1E-6
    } else {
      false
    }

  override fun hashCode() = value.hashCode()
}
