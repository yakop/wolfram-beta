package yg.wolframbeta.grammar

import yg.wolframbeta.compiler.Context

interface Statement {
  fun execute(context: Context): Pair<Expression, Context>
}

abstract class Assignment(private val name: String, protected val value: Expression) : Statement {
  override fun execute(context: Context) =
    processValue().let { it to Context(mapOf(name to it), context) }

  protected abstract fun processValue(): Expression
}

class SimpleAssignment(name: String, value: Expression) : Assignment(name, value) {
  override fun processValue() = Const(value.evaluate())
}

class LazyAssignment(name: String, value: Expression) : Assignment(name, value) {
  override fun processValue() = value
}
