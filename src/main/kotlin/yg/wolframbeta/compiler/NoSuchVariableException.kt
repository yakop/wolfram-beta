package yg.wolframbeta.compiler

class NoSuchVariableException(override val message: String?) : RuntimeException(message)