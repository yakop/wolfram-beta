package yg.wolframbeta.compiler

abstract class Compiler<S> {
  abstract fun compile(input: S): Program
}
