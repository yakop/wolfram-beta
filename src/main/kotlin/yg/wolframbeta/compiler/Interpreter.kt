package yg.wolframbeta.compiler

import yg.wolframbeta.grammar.Expression
import yg.wolframbeta.grammar.Statement

data class Program(private val statements: List<Statement>) {
  fun run(): List<Pair<Expression, Context>> = statements.fold(emptyList()) { accumulated, next ->
    accumulated + next.execute(accumulated.lastOrNull()?.second ?: Context())
  }
}

data class Context(private val itself: Map<String, Expression>) {
  constructor() : this(emptyMap())
  constructor(itself: Map<String, Expression>, context: Context) : this(itself + context.itself)

  operator fun get(name: String) =
    itself[name] ?: throw NoSuchVariableException("Variable $name not defined in this context")
}
