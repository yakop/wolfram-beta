package yg.wolframbeta.xml

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonRootName
import yg.wolframbeta.compiler.Program
import yg.wolframbeta.grammar.*

@JsonRootName("Statements")
data class XmlProgram(@JsonProperty("Statement") val statements: List<XmlStatement>) {
  fun toProgram() = Program(statements.map(this::convertToStatement))

  private fun convertToStatement(xmlStatement: XmlStatement): Statement =
    when (xmlStatement.type) {
      ExpressionTypes.MUL -> with(extractLeftAndRight(xmlStatement)) { Mul(first, second) }
      ExpressionTypes.DIV -> with(extractLeftAndRight(xmlStatement)) { Div(first, second) }
      ExpressionTypes.SUM -> with(extractLeftAndRight(xmlStatement)) { Sum(first, second) }
      ExpressionTypes.DIFF -> with(extractLeftAndRight(xmlStatement)) { Diff(first, second) }
      ExpressionTypes.VAR -> Variable(
        xmlStatement.value ?: throw IllegalArgumentException("Variable is not set!")
      )
      ExpressionTypes.CONST -> Const(
        xmlStatement.value?.toDouble() ?: throw IllegalArgumentException("Const value is not set!")
      )
    }

  private fun extractLeftAndRight(xmlStatement: XmlStatement) =
    if (xmlStatement.statements?.size == 2) {
      convertToStatement(xmlStatement.statements[0]) as Expression to
          convertToStatement(xmlStatement.statements[1]) as Expression
    } else {
      throw IllegalArgumentException("Binary operator should have 2 children!")
    }
}

enum class ExpressionTypes {
  @JsonProperty("Mul")
  MUL,
  @JsonProperty("Div")
  DIV,
  @JsonProperty("Sum")
  SUM,
  @JsonProperty("Diff")
  DIFF,
  @JsonProperty("Var")
  VAR,
  @JsonProperty("Const")
  CONST
}

@JsonRootName("Statement")
data class XmlStatement(
  val type: ExpressionTypes,
  val value: String?,
  val statements: List<XmlStatement>? = emptyList()
)
