package yg.wolframbeta.xml

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import yg.wolframbeta.compiler.Compiler
import yg.wolframbeta.compiler.Program

class StringXmlCompiler : Compiler<String>() {
  private val mapper = XmlMapper(JacksonXmlModule().apply {
    setDefaultUseWrapper(false)
  }).registerKotlinModule()
    .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  override fun compile(input: String) =
    if (input.isBlank()) {
      Program(emptyList())
    } else {
      mapper.readValue<XmlProgram>(input).toProgram()
    }
}