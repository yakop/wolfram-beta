package yg.wolframbeta.operations.flatops

import org.junit.jupiter.api.Test
import yg.wolframbeta.grammar.*
import kotlin.test.assertEquals

class FlatExpressionConverterTest {
  private val converter = FlatExpressionConverter()

  @Test
  fun `for const expression flat expression is equal to initial one`() {
    val initialExpression = Const(5.0)
    val expectedExpression = FlatSum(listOf(Const(5.0)).map { FlatMul(listOf(it)) })
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `for variable expression flat expression is equal to initial one`() {
    val initialExpression = Variable("x")
    val expectedExpression = FlatSum(listOf(Variable("x")).map { FlatMul(listOf(it)) })
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `for sum expression converter returns list of two elements`() {
    val initialExpression = Sum(Const(1.0), Const(2.0))
    val expectedExpression =
      FlatSum(listOf(Const(1.0), Const(2.0)).map { FlatMul(listOf(it)) })
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `for multiple sum expression converter returns list of these elements`() {
    val initialExpression = Sum(Const(1.0), Sum(Const(2.0), Sum(Const(3.0), Const(4.0))))
    val expectedExpression =
      FlatSum(listOf(Const(1.0), Const(2.0), Const(3.0), Const(4.0)).map { FlatMul(listOf(it)) })
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `for diff expression converter returns list of two elements where second is negated`() {
    val initialExpression = Diff(Const(1.0), Const(2.0))
    val expectedExpression =
      FlatSum(
        listOf(
          listOf(Const(1.0)),
          listOf(Const(-1.0), Const(2.0))
        ).map { FlatMul(it) })
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `for multiple diff expression converter returns list of these elements negated properly`() {
    val initialExpression = Diff(Const(1.0), Diff(Const(2.0), Diff(Const(3.0), Const(4.0))))
    val expectedExpression =
      FlatSum(
        listOf(
          listOf(Const(1.0)),
          listOf(Const(-1.0), Const(2.0)),
          listOf(Const(-1.0), Const(-1.0), Const(3.0)),
          listOf(Const(-1.0), Const(-1.0), Const(-1.0), Const(4.0))
        ).map { FlatMul(it) }
      )
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `for sum tree we get sum list`() {
    val initialExpression = Sum(
      Sum(Const(1.0), Sum(Const(2.0), Const(3.0))),
      Sum(Const(4.0), Sum(Const(5.0), Const(6.0)))
    )
    val expectedExpression =
      FlatSum(
        listOf(
          Const(1.0),
          Const(2.0),
          Const(3.0),
          Const(4.0),
          Const(5.0),
          Const(6.0)
        ).map { FlatMul(listOf(it)) }
      )
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `for diff tree we get diff list with negation`() {
    val initialExpression = Diff(
      Diff(Const(1.0), Diff(Const(2.0), Const(3.0))),
      Diff(Const(4.0), Diff(Const(5.0), Const(6.0)))
    )
    val expectedExpression =
      FlatSum(
        listOf(
          listOf(Const(1.0)),
          listOf(Const(-1.0), Const(2.0)),
          listOf(Const(-1.0), Const(-1.0), Const(3.0)),
          listOf(Const(-1.0), Const(4.0)),
          listOf(Const(-1.0), Const(-1.0), Const(5.0)),
          listOf(Const(-1.0), Const(-1.0), Const(-1.0), Const(6.0))
        ).map { FlatMul(it) }
      )
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `mul converts to list of two elements`() {
    val initialExpression = Mul(Const(1.0), Const(2.0))
    val expectedExpression =
      FlatSum(listOf(FlatMul(listOf(Const(1.0), Const(2.0)))))
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `mul tree converts to flat mul`() {
    val initialExpression = Mul(Mul(Const(1.0), Const(2.0)), Mul(Const(3.0), Const(4.0)))
    val expectedExpression =
      FlatSum(listOf(FlatMul(listOf(Const(1.0), Const(2.0), Const(3.0), Const(4.0)))))
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `mul and sum tree converts to sum of flat muls according to arithmetic rules`() {
    val initialExpression = Mul(Sum(Const(1.0), Const(2.0)), Sum(Const(3.0), Const(4.0)))
    val expectedExpression =
      FlatSum(
        listOf(
          listOf(Const(1.0), Const(3.0)),
          listOf(Const(1.0), Const(4.0)),
          listOf(Const(2.0), Const(3.0)),
          listOf(Const(2.0), Const(4.0))
        ).map { FlatMul(it) }
      )
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `deep mul and sum tree converts to sum of flat muls according to arithmetic rules`() {
    val initialExpression =
      Mul(Sum(Const(1.0), Sum(Const(2.0), Const(3.0))), Sum(Const(4.0), Const(5.0)))
    val expectedExpression =
      FlatSum(
        listOf(
          listOf(Const(1.0), Const(4.0)),
          listOf(Const(1.0), Const(5.0)),
          listOf(Const(2.0), Const(4.0)),
          listOf(Const(2.0), Const(5.0)),
          listOf(Const(3.0), Const(4.0)),
          listOf(Const(3.0), Const(5.0))
        ).map { FlatMul(it) }
      )
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

  @Test
  fun `deep mul and sum expressions with variables converts to sum of flat muls according to arithmetic rules`() {
    val initialExpression =
      Sum(
        Sum(Const(6.0), Variable("x")),
        Sum(Mul(Variable("x"), Variable("x")), Sum(Const(5.0), Const(4.0)))
      )
    val expectedExpression =
      FlatSum(
        listOf(
          listOf(Const(6.0)),
          listOf(Variable("x")),
          listOf(Variable("x"), Variable("x")),
          listOf(Const(5.0)),
          listOf(Const(4.0))
        ).map { FlatMul(it) }
      )
    assertEquals(expectedExpression, converter.convert(initialExpression))
  }

//  @Test
//  fun `division expression converts to flat inversion`() {
//    val initialExpression = Div(Const(1.0), Const(2.0))
//    val expectedExpression =
//      FlatSum(listOf(FlatInversion(listOf(Const(1.0), Const(2.0)))))
//    assertEquals(expectedExpression, converter.convert(initialExpression))
//  }
}

