package yg.wolframbeta.operations.simplification

import org.junit.jupiter.api.Test
import yg.wolframbeta.grammar.*
import kotlin.test.assertEquals

class SimplificationTest {
  private val simplifier = ExpressionSimplifier()

  @Test
  fun `for zero simplification result is zero`() {
    val result = simplifier.simplify(Const(0.0))
    assertEquals(Const(0.0), result)
  }

  @Test
  fun `for non-zero constant simplification result is constant itself`() {
    val result = simplifier.simplify(Const(5.0))
    assertEquals(Const(5.0), result)
  }

  @Test
  fun `for variable simplification result is variable itself`() {
    val result = simplifier.simplify(Variable("x"))
    assertEquals(Variable("x"), result)
  }

  @Test
  fun `sum of zero and number is number`() {
    val result = simplifier.simplify(Sum(Const(0.0), Const(5.0)))
    assertEquals(Const(5.0), result)
  }

  @Test
  fun `sum of number and zero is number`() {
    val result = simplifier.simplify(Sum(Const(5.0), Const(0.0)))
    assertEquals(Const(5.0), result)
  }

  @Test
  fun `zero minus variable is equal to negated variable`() {
    val result = simplifier.simplify(Diff(Const(0.0), Variable("x")))
    assertEquals(Mul(Const(-1.0), Variable("x")), result)
  }

  @Test
  fun `number minus zero is equal to number`() {
    val result = simplifier.simplify(Diff(Const(5.0), Const(0.0)))
    assertEquals(Const(5.0), result)
  }

  @Test
  fun `chain sum of zeroes is zero`() {
    val result = simplifier.simplify(
      Sum(
        Sum(Const(0.0), Sum(Const(0.0), Const(0.0))),
        Sum(Const(0.0), Sum(Const(0.0), Sum(Const(0.0), Const(0.0))))
      )
    )
    assertEquals(Const(0.0), result)
  }

  @Test
  fun `negated twice is value itself`() {
    val result = simplifier.simplify(Neg(Neg(Const(5.0))))
    assertEquals(Const(5.0), result)
  }

  @Test
  fun `negated number is -number`() {
    val result = simplifier.simplify(Neg(Const(5.0)))
    assertEquals(Const(-5.0), result)
  }

  @Test
  fun `chain diff of zeroes is zero`() {
    val result = simplifier.simplify(
      Diff(
        Diff(Const(0.0), Diff(Const(0.0), Const(0.0))),
        Diff(Const(0.0), Diff(Const(0.0), Diff(Const(0.0), Const(0.0))))
      )
    )
    assertEquals(Const(0.0), result)
  }

  @Test
  fun `multiplication by zero is zero`() {
    val result1 = simplifier.simplify(Mul(Const(0.0), Const(5.0)))
    val result2 = simplifier.simplify(Mul(Const(5.0), Const(0.0)))
    assertEquals(Const(0.0), result1)
    assertEquals(Const(0.0), result2)
  }

//  @Test
//  fun `division of zero by number is zero`() {
//    val result = simplifier.simplify(Div(Const(0.0), Const(5.0)))
//    assertEquals(Const(0.0), result)
//  }

  @Test
  fun `sum of number and negated number is zero`() {
    val result = simplifier.simplify(Sum(Const(6.0), Neg(Const(6.0))))
    assertEquals(Const(0.0), result)
  }

  @Test
  fun `sum of multiple numbers is one number equal to it's sum`() {
    val result = simplifier.simplify(Sum(Sum(Sum(Const(1.0), Const(2.0)), Const(3.0)), Const(4.0)))
    assertEquals(Const(10.0), result)
  }

  @Test
  fun `diff of multiple numbers is equal to it's diff`() {
    val result =
      simplifier.simplify(Diff(Diff(Diff(Const(4.0), Const(3.0)), Const(2.0)), Const(1.0)))
    assertEquals(Const(-2.0), result)
  }

  @Test
  fun `numbers within expression with variable are folded`() {
    val result = simplifier.simplify(Sum(Const(6.0), Sum(Variable("x"), Const(5.0))))
    assertEquals(Sum(Variable("x"), Const(11.0)), result)
  }

  @Test
  fun `expressions is ordered as following`() {
    val result = simplifier.simplify(
      Sum(
        Sum(Const(6.0), Variable("x")),
        Sum(Mul(Variable("x"), Variable("x")), Sum(Const(5.0), Const(4.0)))
      )
    )
    assertEquals(Sum(Mul(Variable("x"), Variable("x")), Sum(Variable("x"), Const(15.0))), result)
  }

  @Test
  fun `variables in expression are folded`() {
    val result = simplifier.simplify(Sum(Variable("x"), Variable("x")))
    assertEquals(Mul(Const(2.0), Variable("x")), result)
  }

  @Test
  fun `variables in multiplications are folded`() {
    val result = simplifier.simplify(
      Sum(
        Diff(
          Mul(Variable("x"), Variable("x")),
          Sum(Mul(Const(4.0), Variable("x")), Mul(Const(7.0), Mul(Variable("x"), Variable("x"))))
        ),
        Sum(Mul(Const(5.0), Variable("x")), Mul(Variable("y"), Variable("x")))
      )
    )
    assertEquals(
      Sum(
        Mul(Const(-6.0), Mul(Variable("x"), Variable("x"))),
        Sum(Mul(Variable("x"), Variable("y")), Variable("x"))
      ),
      result
    )
  }


  @Test
  fun `pow expression of constants are evaluated`() {
    val result = simplifier.simplify(Pow(Const(5.0), Const(3.0)))
    val expectedExpression = Const(125.0)

    assertEquals(expectedExpression, result)
  }

  @Test
  fun `pow expression with one in exponent is annihilated`() {
    val result = simplifier.simplify(Pow(Sum(Variable("x"), Variable("y")), Const(1.0)))
    val expectedExpression = Sum(Variable("x"), Variable("y"))

    assertEquals(expectedExpression, result)
  }

  @Test
  fun `pow expression with zero in base is evaluated to zero`() {
    val result = simplifier.simplify(Pow(Const(0.0), Variable("x")))
    val expectedExpression = Const(0.0)

    assertEquals(expectedExpression, result)
  }
}
