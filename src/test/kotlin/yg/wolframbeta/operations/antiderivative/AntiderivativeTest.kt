package yg.wolframbeta.operations.antiderivative

import org.junit.jupiter.api.Test
import yg.wolframbeta.grammar.*
import yg.wolframbeta.grammar.Function
import yg.wolframbeta.grammar.PredefinedFunction.PredefinedFunctions
import kotlin.test.assertEquals

class AntiderivativeTest {

  private val evaluator = AntiderivativeEvaluator()

  @Test
  fun `antiderivative of a zero is zero`() {
    val expression = evaluator.findAntiderivative(Function(Const(0.0), emptyMap()), Variable("x"))
    val expectedOutput = Function(Const(0.0), emptyMap())
    assertEquals(expectedOutput, expression)
  }

  @Test
  fun `antiderivative of sum of zeroes is zero`() {
    val expression =
      evaluator.findAntiderivative(Function(Sum(Const(0.0), Const(0.0)), emptyMap()), Variable("x"))
    val expectedOutput = Function(Const(0.0), emptyMap())
    assertEquals(expectedOutput, expression)
  }

  @Test
  fun `antiderivative of a non-zero constant is this constant multiplied by variable`() {
    val expression = evaluator.findAntiderivative(Function(Const(5.0), emptyMap()), Variable("x"))
    val expectedOutput = Function(Mul(Const(5.0), Variable("x")), mapOf(Variable("x") to null))
    assertEquals(expectedOutput, expression)
  }

  @Test
  fun `antiderivative of sum of non-zero constants is sum of that constants multiplied by variable`() {
    val expression =
      evaluator.findAntiderivative(
        Function(
          Sum(Const(5.0), Sum(Const(6.0), Const(7.0))),
          emptyMap()
        ), Variable("x")
      )
    val expectedOutput = Function(Mul(Const(18.0), Variable("x")), mapOf(Variable("x") to null))
    assertEquals(expectedOutput, expression)
  }

  @Test
  fun `antiderivative of sum of non-zero constants and variables is sum of that constants multiplied by variable`() {
    val expression =
      evaluator.findAntiderivative(
        Function(
          Sum(Variable("y"), Sum(Mul(Variable("z"), Const(6.0)), Sum(Const(7.0), Const(8.0)))),
          emptyMap()
        ), Variable("x")
      )
    val expectedOutput = Function(
      Sum(
        Mul(Const(6.0), Mul(Variable("x"), Variable("z"))),
        Sum(Mul(Variable("x"), Variable("y")), Mul(Const(15.0), Variable("x")))
      ),
      mapOf(Variable("x") to null)
    )

    assertEquals(expectedOutput, expression)
  }

  @Test
  fun `antiderivative of trigonometric functions evaluated as follows`() {
    val expression =
      evaluator.findAntiderivative(
        Function(
          Sum(
            PredefinedFunction(PredefinedFunctions.SIN, listOf(Variable("x"))),
            PredefinedFunction(PredefinedFunctions.COS, listOf(Variable("x")))
          ),
          emptyMap()
        ), Variable("x")
      )

    val expectedOutput = Function(
      Sum(
        Mul(
          Const(-1.0),
          PredefinedFunction(PredefinedFunctions.COS, listOf(Variable("x")))
        ),
        PredefinedFunction(PredefinedFunctions.SIN, listOf(Variable("x")))
      ),
      mapOf(Variable("x") to null)
    )

    assertEquals(expectedOutput, expression)
  }

  @Test
  fun `antiderivative of pow evaluated as follows`() {
    val expression =
      evaluator.findAntiderivative(
        Function(
          Sum(
            Mul(Pow(Variable("x"), Const(3.0)), Const(5.0)),
            Mul(Pow(Variable("x"), Const(5.0)), Const(7.0))
          ),
          emptyMap()
        ), Variable("x")
      )

    val expectedOutput = Function(
      Sum(
        Mul(
          Const(1.25),
          Pow(Variable("x"), Const(4.0))
        ),
        Mul(
          Const(7.0 / 6.0),
          Pow(Variable("x"), Const(6.0))
        )
      ),
      mapOf(Variable("x") to null)
    )

    assertEquals(expectedOutput, expression)
  }
}