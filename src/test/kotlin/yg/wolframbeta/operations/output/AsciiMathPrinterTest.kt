package yg.wolframbeta.operations.output

import org.junit.jupiter.api.Test
import yg.wolframbeta.grammar.*
import yg.wolframbeta.grammar.PredefinedFunction.PredefinedFunctions
import kotlin.test.assertEquals

class AsciiMathPrinterTest {

  private val printer = AsciiMathPrinter()

  @Test
  fun `constant printed as is`() {
    val output = printer.operate(Const(5.0))
    assertEquals("5.0", output)
  }

  @Test
  fun `sum of variable and constant printed properly`() {
    val output = printer.operate(Sum(Variable("x"), Const(5.0)))
    assertEquals("x + 5.0", output)
  }

  @Test
  fun `sum of complex arithmetic expression printed properly`() {
    val output = printer.operate(
      Mul(
        Div(Diff(Const(8.0), Variable("y")), Const(6.0)),
        Sum(Variable("x"), Const(5.0))
      )
    )
    assertEquals("(8.0 - y) / 6.0 * (x + 5.0)", output)
  }

  @Test
  fun `complex expression evaluated as follows`() {
    val output = printer.operate(
      Pow(
        PredefinedFunction(PredefinedFunctions.COS, listOf(Variable("x"))),
        Sum(Mul(Const(7.0), Variable("y")), Neg(Const(1.0)))
      )
    )

    assertEquals(
      "cos(x) ^ (7 * y - 1)", output
    )
  }
}