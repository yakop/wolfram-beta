package yg.wolframbeta.grammar

import org.junit.jupiter.api.Test
import yg.wolframbeta.compiler.Context
import yg.wolframbeta.compiler.Program
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ExecutionTest {

  @Test
  fun `for empty program we get no output`() {
    val program = Program(emptyList())
    assertTrue(program.run().isEmpty())
  }

  @Test
  fun `for simple expression program we get output equal to it's evaluation result`() {
    val program = Program(listOf(Const(5.0)))
    val expectedOutput = listOf(Const(5.0) to Context())
    assertEquals(expectedOutput, program.run())
  }

  @Test
  fun `for multiple expression program we get output equal to it's evaluation result`() {
    val program = Program(listOf(Const(5.0), Const(6.0), Sum(Const(5.0), Const(6.0))))
    val expectedOutput =
      listOf(Const(5.0) to Context(), Const(6.0) to Context(), Const(11.0) to Context())
    assertEquals(expectedOutput, program.run())
  }

  @Test
  fun `for simple assignment program we get modified context`() {
    val program = Program(listOf(SimpleAssignment("x", Const(5.0))))
    val expectedOutput = listOf(Const(5.0) to Context(mapOf("x" to Const(5.0))))
    assertEquals(expectedOutput, program.run())
  }

  @Test
  fun `for multiple assignment program we get second context containing first`() {
    val program =
      Program(listOf(SimpleAssignment("x", Const(5.0)), SimpleAssignment("y", Const(6.0))))
    val expectedOutput = listOf(
      Const(5.0) to Context(mapOf("x" to Const(5.0))),
      Const(6.0) to Context(mapOf("x" to Const(5.0), "y" to Const(6.0)))
    )
    assertEquals(expectedOutput, program.run())
  }

  @Test
  fun `for simple lazy assignment program we get modified context`() {
    val program = Program(listOf(LazyAssignment("x", Sum(Const(5.0), Const(6.0)))))
    val expectedOutput =
      listOf(Sum(Const(5.0), Const(6.0)) to Context(mapOf("x" to Sum(Const(5.0), Const(6.0)))))
    assertEquals(expectedOutput, program.run())
  }
}
