package yg.wolframbeta.grammar

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import yg.wolframbeta.compiler.Context
import yg.wolframbeta.compiler.NoSuchVariableException
import kotlin.test.assertEquals

class EvaluationTest {

  @Test
  fun `const evaluates to it's contents`() {
    val expression = Const(5.0)
    assertEquals(5.0, expression.evaluate())
  }

  @Test
  fun `sum expression evaluates to sum of its elements`() {
    val expression = Sum(Const(5.0), Const(6.0))
    assertEquals(11.0, expression.evaluate())
  }

  @Test
  fun `multiple elements sum expression evaluates to sum of its elements`() {
    val expression =
      Sum(
        Const(1.0), Sum(Const(2.0), Sum(Const(3.0), Sum(Const(4.0), Sum(Const(5.0), Const(6.0)))))
      )
    assertEquals(21.0, expression.evaluate())
  }

  @Test
  fun `diff expression of multiple element evaluates to its difference`() {
    val expression = Diff(Const(5.0), Diff(Const(6.0), Const(-7.0)))
    assertEquals(-8.0, expression.evaluate())
  }

  @Test
  fun `sum and diff expression evaluates according to arithmetic rules`() {
    val expression = Sum(Const(9.0), Diff(Const(1.0), Const(6.0)))
    assertEquals(4.0, expression.evaluate())
  }

  @Test
  fun `multiplication expression evaluates according to arithmetic rules`() {
    val expression = Mul(Const(7.0), Const(8.0))
    assertEquals(56.0, expression.evaluate())
  }

  @Test
  fun `complex multiplication expression evaluates according to arithmetic rules`() {
    val expression = Mul(Const(1.0), Mul(Const(2.0), Mul(Const(3.0), Mul(Const(4.0), Const(5.0)))))
    assertEquals(120.0, expression.evaluate())
  }

  @Test
  fun `division expression evaluates according to to arithmetic rules`() {
    val expression = Div(Const(4.0), Const(8.0))
    assertEquals(0.5, expression.evaluate())
  }

  @Test
  fun `complex division expression evaluates according to to arithmetic rules`() {
    val expression = Div(Div(Const(100.0), Const(20.0)), Const(4.0))
    assertEquals(1.25, expression.evaluate())
  }

  @Test
  fun `complex multiplication and division expression evaluates according to to arithmetic rules`() {
    val expression = Mul(Div(Mul(Const(5.0), Const(6.0)), Const(2.0)), Const(15.0))
    assertEquals(225.0, expression.evaluate())
  }

  @Test
  fun `complex expression of all known binary operations evaluates according to to arithmetic rules`() {
    val expression = Div(
      Mul(Sum(Const(5.0), Const(6.0)), Const(2.0)),
      Diff(Mul(Const(9.0), Const(10.0)), Const(2.0))
    )
    assertEquals(0.25, expression.evaluate())
  }

  @Test
  fun `for variable defined in context it's evaluation returns it's value`() {
    val context = Context(mapOf("x" to Const(5.0)))
    val expression = Variable("x")
    assertEquals(5.0, expression.evaluate(context))
  }

  @Test
  fun `for variable not defined in context it's evaluation returns it's value`() {
    val context = Context(emptyMap())
    val expression = Variable("x")
    assertThrows<NoSuchVariableException> { expression.evaluate(context) }
  }

  @Test
  fun `for variable expression evaluation happens just like for numbers`() {
    val context = Context(
      mapOf(
        "x" to Const(5.0),
        "y" to Const(6.0),
        "z" to Const(2.0),
        "w" to Const(9.0),
        "v" to Const(10.0)
      )
    )
    val expression = Div(
      Mul(Sum(Variable("x"), Variable("y")), Variable("z")),
      Diff(Mul(Variable("w"), Variable("v")), Variable("z"))
    )
    assertEquals(0.25, expression.evaluate(context))
  }

  @Test
  fun `negated zero is zero`() {
    val expression = Neg(Const(0.0))
    assertEquals(0.0, expression.evaluate())
  }

  @Test
  fun `negated number is equal to -number`() {
    val expression = Neg(Const(5.0))
    assertEquals(-5.0, expression.evaluate())
  }

  @Test
  fun `negated twice number is equal to itself`() {
    val expression = Neg(Neg(Const(5.0)))
    assertEquals(5.0, expression.evaluate())
  }

  @Test
  fun `inverted number is equal to 1 divided by this number`() {
    val expression = Invert(Const(5.0))
    assertEquals(1.0 / 5.0, expression.evaluate())
  }
}