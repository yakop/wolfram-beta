package yg.wolframbeta.compiler

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import yg.wolframbeta.grammar.*
import yg.wolframbeta.xml.StringXmlCompiler
import java.lang.IllegalArgumentException
import kotlin.test.assertEquals

class StringXmlCompilerTest {

  private val compiler = StringXmlCompiler()

  private fun givenOneSumStatementProgram() = """
    <Statements>
      <Statement>
        <Type>Sum</Type>
        <Statements>
          <Statement>
            <Type>Const</Type>
            <Value>3</Value>
          </Statement>
          <Statement>
            <Type>Const</Type>
            <Value>5</Value>
          </Statement>
        </Statements>
      </Statement>
    </Statements>
    """.trimIndent()

  private fun givenOneDiffStatementProgram() = """
    <Statements>
      <Statement>
        <Type>Diff</Type>
        <Statements>
          <Statement>
            <Type>Const</Type>
            <Value>3</Value>
          </Statement>
          <Statement>
            <Type>Const</Type>
            <Value>5</Value>
          </Statement>
        </Statements>
      </Statement>
    </Statements>
    """.trimIndent()

  private fun givenNestedSumDiffProgram() = """
    <Statements>
      <Statement>
        <Type>Diff</Type>
        <Statements>
          <Statement>
            <Type>Const</Type>
            <Value>3</Value>
          </Statement>
          <Statement>
            <Type>Sum</Type>
            <Statements>
              <Statement>
                <Type>Const</Type>
                <Value>3</Value>
              </Statement>
              <Statement>
                <Type>Const</Type>
                <Value>7</Value>
              </Statement>
            </Statements>
          </Statement>
        </Statements>
      </Statement>
    </Statements>
    """.trimIndent()

  private fun givenMulDivVarProgram() = """
    <Statements>
      <Statement>
        <Type>Mul</Type>
        <Statements>
          <Statement>
            <Type>Var</Type>
            <Value>x</Value>
          </Statement>
          <Statement>
            <Type>Div</Type>
            <Statements>
              <Statement>
                <Type>Var</Type>
                <Value>y</Value>
              </Statement>
              <Statement>
                <Type>Const</Type>
                <Value>2</Value>
              </Statement>
            </Statements>
          </Statement>
        </Statements>
      </Statement>
    </Statements>
  """.trimIndent()

  private fun givenIncorrectProgram() = """
    <Statements>
      <Statement>
        <Type>Mul</Type>
        <Statements/>
      </Statement>
    </Statements>
  """.trimIndent()

  @Test
  fun `for empty input compiler returns empty program object`() {
    assertEquals(Program(emptyList()), compiler.compile(""))
  }

  @Test
  fun `compiler throws an exception`() {
    val program = givenIncorrectProgram()
    assertThrows<IllegalArgumentException> { compiler.compile(program) }
  }

  @Test
  fun `compiler returns program of simple statement`() {
    val program = givenOneSumStatementProgram()
    val expectedProgram = Program(listOf(Sum(Const(3.0), Const(5.0))))
    assertEquals(expectedProgram, compiler.compile(program))
  }

  @Test
  fun `compiler returns program of this statement`() {
    val program = givenOneDiffStatementProgram()
    val expectedProgram = Program(listOf(Diff(Const(3.0), Const(5.0))))
    assertEquals(expectedProgram, compiler.compile(program))
  }

  @Test
  fun `compiler returns correct simple program`() {
    val program = givenNestedSumDiffProgram()
    val expectedProgram = Program(listOf(Diff(Const(3.0), Sum(Const(3.0), Const(7.0)))))
    assertEquals(expectedProgram, compiler.compile(program))
  }

  @Test
  fun `compiler returns correct program`() {
    val program = givenMulDivVarProgram()
    val expectedProgram = Program(listOf(Mul(Variable("x"), Div(Variable("y"), Const(2.0)))))
    assertEquals(expectedProgram, compiler.compile(program))
  }
}